<?php
class MyClass
{
    public $name="A";
    public $id=1;
    public $cgpa=3.5;


    public function setName($name)
    {
        $this->name = $name;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCgpa($cgpa)
    {
        $this->cgpa = $cgpa;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCgpa()
    {
        return $this->cgpa;
    }
}
$obj = new MyClass();
var_dump($obj);
$ob1 = new MyClass();
$ob1->setName("B");
var_dump($ob1);