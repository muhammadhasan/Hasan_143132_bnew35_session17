<?php
class MyClass
{
    public $a = "Hello there";
    public function setName($name)
    {
        $this->a = $name;
    }
    public function getName()
    {
        return $this->a . "<br/>";
    }
}
$ob1 = new MyClass();
$ob2 = new MyClass();
echo $ob1-> getName();
$ob2->setName("Hi");
echo $ob2->getName();
/*$ob1-> setName("Hi");
$ob2-> getName();
echo $ob2;
*/